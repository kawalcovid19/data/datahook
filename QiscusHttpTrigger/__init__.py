import logging
import os
from urllib.parse import urlparse
from urllib.request import urlopen

import azure.functions as func
from azure.storage.blob import BlobServiceClient


WHITELISTED_PHONENUMBER = [
    '628111505415' # rendy b junior
]

def main(req: func.HttpRequest) -> func.HttpResponse:

    req_body = req.get_json()
    phone_number = req_body.get("payload").get("from").get("email")
    message = req_body.get("payload").get("message")
    message_type = message.get("type")
    message_timestamp = message.get("timestamp")

    if phone_number in WHITELISTED_PHONENUMBER:
        file_name = f"{phone_number} {message_type} {message_timestamp}"
        file_bytes = ""

        if message_type == "text":
            file_name += ".txt"
            file_bytes = message.get("text")
        elif message_type == "file_attachment":
            # download the image
            file_url = message.get("payload").get("url")
            response = urlopen(file_url)
            file_bytes = response.read()

            # prepare file name
            a = urlparse(file_url)
            file_name += " " + os.path.basename(a.path)

        logging.info(file_name)

        # write to storage
        connect_str = os.environ["AZURE_STORAGE_CONNECTION_STRING"]
        blob_service_client = BlobServiceClient.from_connection_string(connect_str)
        container_name = os.environ["AZURE_STORAGE_CONTAINER_NAME"]
        blob_client = blob_service_client.get_blob_client(
            container=container_name, blob=file_name
        )
        blob_client.upload_blob(file_bytes)

        return func.HttpResponse(file_name)
